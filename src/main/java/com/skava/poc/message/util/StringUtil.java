/*******************************************************************************
 * Copyright ©2002-2014 Skava. 
 * All rights reserved.The Skava system, including 
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and 
 * other laws. Use without permission is prohibited.
 * 
 *  For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.poc.message.util;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil
{
    static Logger logger = LoggerFactory.getLogger(StringUtil.class);

    private static Pattern patternReplacement = Pattern.compile("\\{\\$(\\d+)\\}", Pattern.DOTALL);
    private static Pattern specialCharsPattern = Pattern.compile("[^\\w]", Pattern.DOTALL);
    private static InetAddress localhost = null;
    private static Pattern csvSplitPattern = Pattern.compile("(?x) ,(?=( [^\"] * \"  [^\"] * \" )* [^\"] *$)");
    private static Pattern psvSplitPattern = Pattern.compile("(?x) |(?=( [^\"] * \"  [^\"] * \" )* [^\"] *$)");
    private static Pattern tsvSplitPattern = Pattern.compile("(?x) \t(?=( [^\"] * \"  [^\"] * \" )* [^\"] *$)");
    static
    {
        try
        {
            localhost = InetAddress.getLocalHost();
        }
        catch (Exception e)
        {
            logger.error("Error on getting LocalHost", e);
        }
    }

    public static int indexOfIgnoreCase(String source,
                                        String targetLowerCase,
                                        int fromIndex)
    {
        int sourceCount = source.length();
        int targetCount = targetLowerCase.length();
        int targetOffset = 0;
        int sourceOffset = 0;
        if (fromIndex >= sourceCount) { return (targetCount == 0 ? sourceCount : -1); }
        if (fromIndex < 0)
        {
            fromIndex = 0;
        }
        if (targetCount == 0) { return fromIndex; }

        char first = targetLowerCase.charAt(targetOffset);
        int i = sourceOffset + fromIndex;
        int max = sourceOffset + (sourceCount - targetCount);

        startSearchForFirstChar: while (true)
        {
            /* Look for first character. */
            while (i <= max && Character.toLowerCase(source.charAt(i)) != first)
            {
                i++;
            }

            if (i > max) { return -1; }

            /* Found first character, now look at the rest of v2 */
            int j = i + 1;
            int end = j + targetCount - 1;
            int k = targetOffset + 1;
            while (j < end)
            {
                if (Character.toLowerCase(source.charAt(j++)) != targetLowerCase.charAt(k++))
                {
                    i++;
                    /* Look for str's first char again. */
                    continue startSearchForFirstChar;
                }
            }
            return i - sourceOffset; /* Found whole string. */
        }
    }

    public static String[] getStrings(String str, String delim, boolean trim)
    {
        String[] strs = null;
        if (str != null)
        {
            StringTokenizer strTok = new StringTokenizer(str, delim);
            strs = new String[strTok.countTokens()];
            for (int i = 0; i < strs.length; i++)
            {
                strs[i] = strTok.nextToken();
                if (trim && strs[i] != null)
                {
                    strs[i] = strs[i].trim();
                }
            }
        }
        return strs;
    }

    // This is how escape split works    
    /*        (?x) "                     + // enable comments, ignore white spaces
    delim                        + // match a delimiter
        "(?="                     + // start positive look ahead
        "  ( "                       + //   start group 1
        "   [^\"]*                  + //     match 'otherThanQuote' zero or more times
        "    [^\"]                    + //     match 'quotedString'
        "  )*                        + //   end group 1 and repeat it zero or more times
        "  %s*                    + //   match 'otherThanQuote'
        "  $                        + // match the end of the string
        ")                           + // stop positive look ahead
    */

    public static String[] csvEscapeSplit(String str)
    {

        return escapeSplit(str, csvSplitPattern);
    }

    public static String[] psvEscapeSplit(String str, Pattern pattern)
    {
        return escapeSplit(str, psvSplitPattern);
    }

    public static String[] tsvEscapeSplit(String str, Pattern pattern)
    {
        return escapeSplit(str, tsvSplitPattern);
    }

    //TODO:MERCHANDIZE done - split into three also patterns - done
    public static String[] escapeSplit(String str, Pattern pattern)
    {
        ArrayList<String> toRetList = null;
        String[] tokens = pattern.split(str);
        if (tokens.length > 0)
        {
            toRetList = new ArrayList<String>();
            for (String t : tokens)
            {
                if (t.length() > 0)
                {
                    if (t.charAt(0) == '"' && t.charAt(t.length() - 1) == '"')
                    {
                        t = t.substring(1, t.length() - 1);
                        toRetList.add(t);
                    }
                    else if (!(t.charAt(0) == '"') && !(t.charAt(t.length() - 1) == '"'))
                    {
                        toRetList.add(t);
                    }
                }
            }
            return (toRetList.size() > 0) ? toRetList.toArray(new String[toRetList.size()]) : null;
        }
        return null;

    }

    public static String[] getStringsWithDelims(String str,
                                                String delim,
                                                boolean returnDelimiters)
    {
        String[] strs = null;
        if (str != null)
        {
            StringTokenizer strTok = new StringTokenizer(str, delim, returnDelimiters);
            strs = new String[strTok.countTokens()];
            for (int i = 0; i < strs.length; i++)
            {
                strs[i] = strTok.nextToken();
            }
        }
        return strs;
    }

    public static String mergeStrings(String[] strs,
                                      String delim,
                                      String defaultVal)
    {
        String str = defaultVal;
        if (strs != null)
        {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < strs.length; i++)
            {
                if (i != 0)
                {
                    strBuf.append(delim);
                }
                strBuf.append(strs[i]);
            }
            str = strBuf.toString();
        }
        return str;
    }

    public static String mergeStrings(String[] strs, String delim)
    {
        return mergeStrings(strs, delim, null);
    }

    public static String mergeObjects(Object[] strs,
                                      String delim,
                                      String delimReplace)
    {
        String str = null;
        if (strs != null)
        {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < strs.length; i++)
            {
                if (i != 0)
                {
                    strBuf.append(delim);
                }
                strBuf.append((delimReplace == null ? strs[i] : (strs[i] == null ? strs[i] : (strs[i].toString()).replace(delim, delimReplace))));
            }
            str = strBuf.toString();
        }
        return str;
    }

    public static String mergeLongArrayToString(long[] values, String delim)
    {
        String str = null;
        if (values != null)
        {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < values.length; i++)
            {
                if (i != 0)
                {
                    strBuf.append(delim);
                }
                strBuf.append(values[i]);
            }
            str = strBuf.toString();
        }
        return str;
    }

    public static String getSubstring(String strVal,
                                      String defaultValue,
                                      int maxLen,
                                      String suffix)
    {
        String value = ReadUtil.getString(strVal, defaultValue);

        if (value != null && value.length() > maxLen)
        {
            value = value.substring(0, maxLen);
            if (suffix != null)
            {
                value = value + suffix;
            }
        }
        return value;
    }

    public static String appendByteArray(byte[] bytes)
    {
        StringBuffer sb = new StringBuffer();
        if (bytes != null)
        {
            for (int i = 0; i < bytes.length; i++)
            {
                sb.append(bytes[i]);
            }
        }

        return sb.toString();
    }

    public static String replaceMultipleStrings(String str,
                                                Map<Pattern, String> regexReplacementMap)
    {
        if (regexReplacementMap != null)
        {
            Iterator<Map.Entry<Pattern, String>> iter = regexReplacementMap.entrySet().iterator();
            while (iter.hasNext())
            {
                Map.Entry<Pattern, String> entry = iter.next();
                Matcher m = entry.getKey().matcher(str);

                StringBuffer sb = new StringBuffer();
                while (m.find())
                {
                    m.appendReplacement(sb, replacePattern(entry.getValue(), m));
                }
                m.appendTail(sb);
                str = sb.toString();
            }
        }
        return str;
    }

    private static String replacePattern(String str, Matcher min)
    {
        Matcher m = patternReplacement.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (m.find())
        {
            m.appendReplacement(sb, min.group(Integer.parseInt(m.group(1))));
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public static boolean stringMatch(String srcString, String descString)
    {
        boolean toRet = false;
        if (srcString != null && descString != null)
        {
            toRet = srcString.equals(descString);
        }
        else
        {
            toRet = true;
        }
        return toRet;
    }

    public static boolean patternMatch(Pattern partnerPattern, String stringVal)
    {
        boolean toRet = false;
        if (stringVal == null || stringVal.equalsIgnoreCase("null"))
        {
            if (partnerPattern == null || partnerPattern.toString().equalsIgnoreCase("null"))
            {
                toRet = true;
            }
            else
            {
                toRet = false;
            }
        }
        else
        {
            //stringVal is not null
            if (partnerPattern == null || partnerPattern.toString().equalsIgnoreCase("null"))
            {
                toRet = false;
            }
            else
            {
                Matcher m = partnerPattern.matcher(stringVal);
                toRet = m.matches();
            }
        }
        return toRet;
    }

    public static String getRandomString()
    {
        String s = System.currentTimeMillis() + localhost.getHostName() + localhost.getHostAddress() + RandomUtils.nextInt();
        return specialCharsPattern.matcher(s).replaceAll("_");
    }

    public static String addStrings(String... strings)
    {
        String toRet = null;
        if (strings != null)
        {
            StringBuffer sb = new StringBuffer();
            for (String str : strings)
            {
                sb.append(str);
            }
            toRet = sb.toString();
        }
        return toRet;
    }

    public static String removeSpecialChars(String str)
    {
        String strTrimmed = null;
        if (str != null)
        {
            strTrimmed = str.toLowerCase().replaceAll("([^a-z0-9\\._])", "");
            strTrimmed = strTrimmed.replaceAll("(.*)(\\.)$", "$1");
        }
        return strTrimmed;
    }

    public static String stripHtmlTagsAndEntities(String text)
    {
        String toRet = null;
        if (text != null)
        {
            toRet = text.replaceAll("\\s*(&(.){0,7};|,|<[^>]*>|\\s)\\s*", " ");
        }
        return toRet;
    }

    public static byte[] getTrimmedString(byte[] content,
                                          String startToken,
                                          String endToken)
    {
        byte[] dataToRet = (content == null ? (new String()).getBytes() : content);

        try
        {
            if (content != null)
            {
                String strToRet = new String(content, "UTF-8");
                int startIdx = 0;
                int endIdx = strToRet.length();

                if (startToken != null && strToRet.contains(startToken))
                {
                    startIdx = strToRet.indexOf(startToken) + startToken.length();
                }

                if (endToken != null)
                {
                    endIdx = strToRet.indexOf(endToken, startIdx);
                }

                if (startIdx != -1 && endIdx != -1 && (startIdx > 0 || endIdx < strToRet.length()))
                {
                    strToRet = strToRet.substring(startIdx, endIdx);
                }
                dataToRet = strToRet.getBytes();
            }
        }
        catch (Exception e)
        {
            logger.error("Error on getTrimmedString ", e);
        }
        return dataToRet;
    }
    

    public static String arrayToCommaseparatedString(String[] strs, String delim)
    {        
        return arrayToCommaseparatedString(strs, delim, null);
    }
    
    public static String arrayToCommaseparatedString(String[] strs,
                                                     String delim,
                                                     String defaultVal)
    { 
        String str = defaultVal;
        if (strs != null)
        {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < strs.length; i++)
            {
                if (i >= 0)
                {
                    String s = strs[i];
                    strs[i] = s.replace(",", "\\,");
                    if(i > 0) { strBuf.append(delim); }                    
                }
                strBuf.append(strs[i]);
            }
            str = strBuf.toString();
        }
        return str;
    }

}
