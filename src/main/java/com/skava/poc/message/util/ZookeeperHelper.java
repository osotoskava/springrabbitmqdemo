package com.skava.poc.message.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.lang.reflect.Constructor;

import org.apache.curator.CuratorZookeeperClient;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.CuratorFrameworkFactory.Builder;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.PreDestroy;

public class ZookeeperHelper implements CuratorWatcher, ApplicationEventPublisherAware, ConfigManager
{
    private Logger logger = LoggerFactory.getLogger(ZookeeperHelper.class);
    private String zkConnect = null;
    private String zkDefaultBasePath = null;
    private String zkBasePath = null;
    private String zkAuthInfo = null;
    private CuratorZookeeperClient zkClient;
    private CuratorFramework cFramework;
    @Getter @Setter private HashMap<String, String> watchableProperties;
    private int timeout = 1000;
    @Getter @Setter private ArrayList<String> propTrees;
    private ApplicationEventPublisher publisher;
    @Getter HashMap<String, String> properties = new HashMap<String, String>();
    @Getter @Setter private Properties globalProperties;

    public ZookeeperHelper(String zkPropPath,
                           String pathSuffix) throws Exception
    {
        FileInputStream fis = new FileInputStream(new File(zkPropPath));
        globalProperties = new Properties();
        globalProperties.load(fis);
        
        
        String defBasePathProp = globalProperties.getProperty("defaultbasepath");
        
        if (defBasePathProp != null && defBasePathProp.length() > 0)
        {
            zkDefaultBasePath = defBasePathProp + "/" + pathSuffix;
        }

        zkConnect = globalProperties.getProperty("zookeeper");
        zkBasePath = globalProperties.getProperty("basepath") + "/" + pathSuffix;
        zkAuthInfo = globalProperties.getProperty("authinfo");

        RetryPolicy retryPolicy = new ExponentialBackoffRetry(timeout, 5);
        Builder curatorBuilder = CuratorFrameworkFactory.builder().connectString(zkConnect).retryPolicy(retryPolicy);
        if (zkAuthInfo != null && zkAuthInfo.length() > 0)
        {
        	curatorBuilder.authorization("digest", zkAuthInfo.getBytes());
        }
        cFramework = curatorBuilder.build();
        cFramework.start();
        watchableProperties = new HashMap<String, String>();
        propTrees = new ArrayList<String>();
    }

    public void setApplicationEventPublisher(ApplicationEventPublisher publisher)
    {
        this.publisher = publisher;
    }

    public HashMap<String, String> loadFromZk() throws IOException, KeeperException, InterruptedException
    {
        List<String> allChildren = new ArrayList<String>();
        List<String> defChildren = null;
        try
        {
            if (zkDefaultBasePath != null && zkDefaultBasePath.length() > 0 && cFramework.checkExists().forPath(zkDefaultBasePath) != null)
            {
                defChildren = cFramework.getChildren().forPath(zkDefaultBasePath);
	            allChildren.addAll(defChildren);
	        }        
            List<String> children = cFramework.getChildren().forPath(zkBasePath);
	        allChildren.addAll(children);
	        for (int i = 0; i < allChildren.size(); i++)
	        {
	            String key = allChildren.get(i);
	            String data = getDataFromKey(key);
	            properties.put(key, data);
	        }
        }
        catch (Exception e) {
			logger.error("ERROR on load from zk ",e);
		}
        
        return properties;
    }

    private String getDataFromKey(String key) throws KeeperException, InterruptedException, IOException
    {
        String data = null;
        try
        {
            boolean watch = false;
            if (getEventForWatchableProperties(key) != null)
            {
                watch = true;
            }
            if (propTrees.contains(key))
            {
                List<String> childrenProps = null;
                if (cFramework.checkExists().forPath(zkBasePath + "/" + key) != null)
                {
                    if (watch)
                    {
                        childrenProps = cFramework.getChildren().usingWatcher(this).forPath(zkBasePath + "/" + key);
                    }
                    else
                    {
                        childrenProps = cFramework.getChildren().forPath(zkBasePath + "/" + key);
                    }
                }
                else if (zkDefaultBasePath != null && zkDefaultBasePath.length() > 0)
                {
                    if (watch)
                    {
                        childrenProps = cFramework.getChildren().usingWatcher(this).forPath(zkDefaultBasePath + "/" + key);
                    }
                    else
                    {
                        childrenProps = cFramework.getChildren().forPath(zkDefaultBasePath + "/" + key);
                    }
                }

                ArrayList<String> childProps = new ArrayList<String>();
                for (String prop : childrenProps)
                {
                    childProps.add(key + "/" + prop);
                }
                data = StringUtil.mergeObjects(childProps.toArray(), ",", null);
            }
            else
            {
                byte[] value = getDataFromZk(key, watch);
                data = new String(value);
            }
        }
        catch (Exception e)
        {
            logger.error("Error on getdata from key ", e);
        }
        return data;
    }

    public String get(String key)
    {
        return get(key, false);
    }
    
    public String get(String key, boolean skipCache)
    {
        try
        {
            if (skipCache)
            {
                boolean watch = false;
                if (getEventForWatchableProperties(key) != null)
                {
                    watch = true;
                }
                return new String(getDataFromZk(key, watch));
            }
            else
            {
                if (properties.containsKey(key))
                {
                    return properties.get(key);
                }
            }
        }
        catch (Exception e)
        {
        	logger.error("Error on get property from zookeeper prop key : "+key,e);
        }
        return null;
    }

    public byte[] getDataFromZk(String key,
                                boolean watch) throws IOException, KeeperException, InterruptedException
    {
        byte[] value = null;
        try
        {
            String path = null;
            path = zkBasePath + "/" + key;
            if (cFramework.checkExists().forPath(path) != null)
            {
                if (watch)
                {
                    value = cFramework.getData().usingWatcher(this).forPath(path);
                }
                else
                {
                    value = cFramework.getData().forPath(path);
                }
            }
            else
            {
                if (zkDefaultBasePath != null && zkDefaultBasePath.length() > 0)
                {
                    path = zkDefaultBasePath + "/" + key;
                    if (cFramework.checkExists().forPath(path) != null)
                    {
                        value = cFramework.getData().forPath(path);
                    }
                }
            }
        }
        catch (Exception t)
        {
            logger.error("Error on getDataFromZk with key " + key, t);
        }

        return value;
    }

    private String getEventForWatchableProperties(String key)
    {
        for (String str : watchableProperties.keySet())
        {
            if (key.matches(str)) { return watchableProperties.get(str); }
        }
        return null;
    }

    public void process(WatchedEvent event)
    {
        String eventPath = event.getPath();
        if (eventPath != null)
        {
            logger.info("Zookeeper Event Triggered For Path: " + eventPath);
            boolean isDefaultBasePath = (zkDefaultBasePath != null) ? eventPath.startsWith(zkDefaultBasePath) : false;
            String basePath = isDefaultBasePath ? zkDefaultBasePath : zkBasePath;
            String key = eventPath.substring(basePath.length() + 1);
            try
            {
                //TODO: To be remove watcher for default basepath property and remove below condition, if property exists in basepath
                if (isDefaultBasePath && (cFramework.checkExists().forPath(zkBasePath + "/" + key) != null))
                {
                    logger.info("Zookeeper Event Skipped For Path: " + eventPath);
                    return;
                }
                String eventName = getEventForWatchableProperties(key);
                String value = getDataFromKey(key);
                Constructor<?> constructor = Class.forName("com.skava.zkevent." + eventName).getConstructor(Object.class, String.class, String.class);
                ApplicationEvent eventObj = (ApplicationEvent) constructor.newInstance(this, key, value);
                publisher.publishEvent(eventObj);
            }
            catch (Exception e)
            {
                logger.error("Error reading data from zookeeper", e);
            }
        }

    }

    public void setPath(String path)
    {
        this.zkBasePath = path;
    }

    public void setZkConnect(String zkConnect)
    {
        this.zkConnect = zkConnect;
    }   

    @PreDestroy
    public void close() throws Exception
    {
        logger.info("Zookeeper Destrory Triggered !!! ");
        try
        {
        	ZooKeeper zk = cFramework.getZookeeperClient().getZooKeeper();
            if(zk != null)
            {
                zk.close();
                logger.info("Zookeeper Destrory zk !!! ");
            }
            if(cFramework != null)
            {
                cFramework.close();
                logger.info("Zookeeper Destrory cFramework !!! ");
            }
        }
        catch (Exception e)
        {
            throw new Exception(e);
        }
    }

	public Properties getGlobalProperties() {
		return globalProperties;
	}

	public void setGlobalProperties(Properties globalProperties) {
		this.globalProperties = globalProperties;
	}
}
