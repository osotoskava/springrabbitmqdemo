/*******************************************************************************
 * Copyright ©2002-2014 Skava. 
 * All rights reserved.The Skava system, including 
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and 
 * other laws. Use without permission is prohibited.
 * 
 *  For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.poc.message.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadUtil
{    
    static Logger logger = LoggerFactory.getLogger(ReadUtil.class);
    public static Pattern PARAM_PATTERN = Pattern.compile("\\{\\@([^\\}]+)\\}");
    public static final String ENCODING_UTF8 = "UTF-8";
    
    private static Random random = new Random(System.currentTimeMillis());
    private static int DEFAULT_BOUND=10000;
    
    public static int getInt(String strVal, int defaultValue)
    {
        int value = defaultValue;
        if (strVal != null)
        {
            try
            {
                value = Integer.parseInt(strVal);
            }
            catch (Exception e)
            {}
        }
        return value;
    }
    
    public static int getInt(Object object, int defaultValue)
    {
        int value = defaultValue;
        if (object != null)
        {
            value = getInt(object.toString(), defaultValue);
        }
        return value;
    }

    public static long getLong(String strVal, long defaultValue)
    {
        long value = defaultValue;
        if (strVal != null)
        {
            try
            {
                value = Long.parseLong(strVal.trim());
            }
            catch (Exception e)
            {}
        }
        return value;
    }
    
    public static long getLong(Object object, long defaultValue)
    {
        long value = defaultValue;
        if (object != null)
        {
            value = getLong(object.toString(), defaultValue);
        }
        return value;
    }
    

    public static float getFloat(String strVal, float defaultValue)
    {
        float value = defaultValue;
        if (strVal != null)
        {
            try
            {
                value = Float.parseFloat(strVal);
            }
            catch (Exception e)
            {}
        }
        return value;
    }
    
    public static float getFloat(Object object, float defaultValue)
    {
        float value = defaultValue;
        if(object!=null)
        {
            value = getFloat(object.toString(), defaultValue);
        }
        return value;
    }
    
    public static double getDouble(String strVal, double defaultValue)
    {
        double value = defaultValue;
        if (strVal != null)
        {
            try
            {
                value = Double.parseDouble(strVal);
            }
            catch (Exception e)
            {}
        }
        return value;
    }
    
    public static double getDouble(Object object, double defaultValue)
    {
        double value = defaultValue;
        if (object != null)
        {
            value = getDouble(object.toString(), defaultValue);
        }
        return value;
    }

    public static boolean getBoolean(String strVal, boolean defaultValue)
    {
        boolean value = defaultValue;
        if (strVal != null)
        {
            try
            {
                value = Boolean.parseBoolean(strVal);
            }
            catch (Exception e)
            {}
        }
        return value;
    }

    public static boolean getBoolean(Object object, boolean defaultValue)
    {
        boolean value = defaultValue;
        if (object != null)
        {
            value = getBoolean(object.toString(), defaultValue);
        }
        return value;
    }

    
    public static String getString(String strVal, String defaultValue)
    {
        String value = defaultValue;
        if (strVal != null && ((strVal = strVal.trim()).length() > 0))
        {
            value = strVal;
        }
        return value;
    }
    
    public static String getString(Object strObj, String defaultValue) 
    {
        String result = defaultValue;
        if(strObj != null) 
        {
            result = getString((String) strObj, defaultValue);
        }
        return result;
    }

    public static String getSubstring(String strVal,
                                      String defaultValue,
                                      int maxLen,
                                      String suffix)
    {
        String value = getString(strVal, defaultValue);

        if (value != null && value.length() > maxLen)
        {
            value = value.substring(0, maxLen);
            if (suffix != null)
            {
                value = value + suffix;
            }
        }
        return value;
    }

    public static String formatString(String strVal, String defaultValue)
    {
        String value = defaultValue;
        if (strVal != null && ((strVal = strVal.trim()).length() > 0))
        {
            //Removing "double quotes" at the begining and end of each field.
            if (strVal.charAt(0) == '"' && strVal.charAt(strVal.length() - 1) == '"')
            {
                value = strVal.substring(1, strVal.length() - 1);
            }
            else
            {
                value = strVal;
            }
        }
        return value;
    }
    
    public static byte[] readStream(InputStream is, int maxToRead) throws Exception
    {
        ByteArrayOutputStream baos = null;
        if (is != null)
        {
            try
            {
                byte[] buffer = new byte[(maxToRead < 0 || maxToRead >= 100000) ? 100000 : maxToRead];
                while ((maxToRead < 0 ? true : (baos == null || baos.size() < maxToRead)))
                {
                    int read = is.read(buffer);
                    if (read < 0)
                    {
                        break;
                    }
                    // create only if there is content - this is important - callers depend on getting null otherwise
                    if (baos == null)
                    {
                        baos = new ByteArrayOutputStream();
                    }
                    baos.write(buffer, 0, read);
                }
            }
            /*
             * catch(ServerException se) { throw se; }
             */
            catch (Exception e)
            {
                throw new Exception(e);
            }
        }
        return (baos == null ? null : baos.toByteArray());
    }
    
    public static Object parseCommaSeparatedValues(String str, int type)
    {
        //        _DBGPRINT_("parsing  str=" + str, null);
        Object retVal = null;
        int len = 0;
        if (str != null)
        {
            int strlen = str.length();
            for (int i = 0; i < strlen; i++)
            {
                if (i == strlen - 1 || str.charAt(i) == ',')
                {
                    len++;
                }
            }

            if (type == Constants.TYPE_CHAR)
            {
                retVal = new char[len];
            }
            else if (type == Constants.TYPE_INT)
            {
                retVal = new int[len];
            }
            else if (type == Constants.TYPE_STRING)
            {
                retVal = new String[len];
            }

            int start = 0;
            int j = 0;
            for (int i = 0; i < strlen; i++)
            {
                if (i == strlen - 1 || str.charAt(i) == ',')
                {
                    String substr = (str.substring(start, (i == strlen - 1 ? strlen : i))).trim();
                    if (type == Constants.TYPE_CHAR)
                    {
                        ((char[]) retVal)[j++] = (substr.length() > 0 && substr.charAt(0) == '%' ? (char) Integer.parseInt(substr.substring(1), 16) : substr.charAt(0));
                    }
                    else if (type == Constants.TYPE_INT)
                    {
                        ((int[]) retVal)[j++] = Integer.parseInt(substr);
                    }
                    else if (type == Constants.TYPE_STRING) //additions on 080206
                    {
                        ((String[]) retVal)[j++] = substr;
                    }
                    start = i + 1;
                }
            }
        }
        return retVal;
    }
    
    @SuppressWarnings("rawtypes")
    public static Object getFirstItem(ArrayList list)
    {
        Object toRet = null;
        if(list != null && list.size() > 0)
        {
            toRet = list.get(0);
        }
        return toRet;
    }
    
    //Confirms .yy format for price fields by padding 0's.
    public static String formatDecimals(String strPrice) 
    {
         if(strPrice == null) return null;
         StringBuilder retPrice = new StringBuilder(strPrice);
         if(strPrice.indexOf(".") > 0) 
         {
             String strDecimal = strPrice.substring(strPrice.indexOf(".")+1);            
             if(strDecimal.length() == 1) 
             {
                 retPrice.append("0");
             }
         } else 
         {
             retPrice.append(".00");
         }
         return retPrice.toString();
     }
    
    public static Map<String, String[]> getParameterMap(String qryString)
    {
        Map<String, String[]> qryStringMap = new HashMap<String, String[]>();

        if (qryString != null && qryString.length() > 0)
        {
            String[] params = qryString.split("&");

            for (String param : params)
            {
                String name = param.split("=")[0];
                String value = param.split("=").length > 1 ? param.split("=")[1] : "";
                String[] tempString = { value };

                qryStringMap.put(name, tempString);
            }
        }

        return qryStringMap;
    }

    public static String buildQueryStringByMap(Map<String, String[]> requestParam)
    {
        String qryString = "";

        if (requestParam != null && requestParam.size() > 0)
        {
            Iterator<Entry<String, String[]>> it = requestParam.entrySet().iterator();
            while (it.hasNext())
            {
                Map.Entry<String, String[]> entry = (java.util.Map.Entry<String, String[]>) it.next();
                String paramName = entry.getKey();
                String[] paramValues = entry.getValue();
                if(paramValues.length > 0)
                {
                    for(String pValue : paramValues)
                    {
                        if (qryString.length() > 0)
                        {
                            qryString += "&" + paramName + "=" + pValue;
                        }
                        else
                        {
                            qryString = paramName + "=" + pValue;
                        }
                    }
                }
                else
                {
                    if (qryString.length() > 0)
                    {
                        qryString += "&" + paramName + "=";
                    }
                    else
                    {
                        qryString = paramName + "=";
                    }
                }
            }
        }
        
        return qryString;
    }
    //TODO:MERCHANDIZE done sri 
    public static String getMacro(String strData)
    {
        if(strData != null && strData.length() > 0)
        {
            Matcher m = PARAM_PATTERN.matcher(strData);
            if(m.find())
            {
                return m.group(1);
            }
        }
        return null;
    }
    
    public static boolean deleteDir(File dir, boolean deleteRoot) 
    {
        if (dir.isDirectory()) 
        {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) 
            {
                boolean success = deleteDir(new File(dir, children[i]), true);
                if (!success) 
                {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return (deleteRoot ? dir.delete() : true);
    }
    public static byte[] readFile(String file) throws IOException
    {
        return readFile(file, false);
    }

    public static byte[] readFile(String file, boolean useEncoding) throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(file != null)
        {
            byte[] bytes = new byte[20000];
            InputStream fis = new BufferedInputStream(new FileInputStream(file));
            try
            {
                int read = 0;
                do
                {
                    read = fis.read(bytes, 0, bytes.length);
                    if(read > 0)
                    {
                        baos.write(bytes, 0, read);
                    }
                }
                while(read >= 0);
            }
            finally
            {                
                fis.close();
            }                
        }
        if(useEncoding)
        {
            return baos.toString(ENCODING_UTF8).getBytes();
        }
        return baos.toByteArray();
    }
    public static <T> T[] arrayAppend(T[] array, T[] elements)
    {
        if(array == null && elements == null)
        {
            return null;
        }
        else if(array == null && elements != null)
        {
            return elements;
        }
        final int N = array.length;
        if(elements != null && elements.length > 0)
        {
            array = Arrays.copyOf(array, N + elements.length);
            for(int i = 0; i < elements.length; i++)
            {
                array[N + i] = elements[i];
            }
        }
        return array;
    }
    
    public static boolean isDouble(String str)
    {
        if (str != null)
        {
            try
            {
                Double.parseDouble(str);
                return true;
            }
            catch (NumberFormatException e)
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public static float roundTwoDecimals(float d)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Float.valueOf(twoDForm.format(d));
    }
    
    /*public static String getGuid() throws DetailedServerException
    {
        String guid;
        int maxGuidLen = ConfigManagerInstance.getInteger("maxGuidLength", 0);
        if (maxGuidLen > 0)
        {
            String str = String.valueOf(System.currentTimeMillis());
            int len = str.length();
            if (maxGuidLen - len < 8) throw new DetailedServerException(ErrorCode.INVALID_GUID_LEN);
            StringBuilder strbuf = new StringBuilder(maxGuidLen);
            strbuf.append(str);
            for (int i = len; i < maxGuidLen; i++)
            {
                Random rand = ((i % 2 == 0) ? random : new Random(System.currentTimeMillis() + getRandomInt(10000)));
                char c = (char) ('a' + getRandomInt(rand, 26));
                strbuf.append(c);
            }
            guid = strbuf.toString();
        }
        else
        {
            guid = UUID.randomUUID().toString();
        }
        return guid;
    }*/
//
//    public static String getGuid() throws Exception
//    {
//        String guid;
//        int maxGuidLen = ConfigManagerInstance.getInteger("maxGuidLength", 0);
//        boolean isDigitsOnly = ConfigManagerInstance.getBoolean("isGuidDigitsOnly", false);
//        
//        if(isDigitsOnly) {
//            String str = String.valueOf(System.currentTimeMillis());
//            int len = str.length();
//            if (maxGuidLen - len < 5) throw new Exception();
//            StringBuilder strbuf = new StringBuilder(maxGuidLen);
//            strbuf.append(str);
//            for (int i = len; i < maxGuidLen; i++)
//            {
//                Random rand = ((i % 2 == 0) ? random : new Random(System.currentTimeMillis() + getRandomInt(10000)));
//                char c = (char) ('0' + getRandomInt(rand, 10));
//                strbuf.append(c);
//            }
//            guid = strbuf.toString();
//        }
//        else if (maxGuidLen > 0)
//        {
//            String str = Long.toHexString(System.currentTimeMillis());
//            int len = str.length();
//            if (maxGuidLen - len < 2) throw new DetailedServerException(ErrorCode.INVALID_GUID_LEN);
//            StringBuilder strbuf = new StringBuilder(maxGuidLen);
//            strbuf.append(str);
//            for (int i = len; i < maxGuidLen; i++)
//            {
//                Random rand = ((i % 2 == 0) ? random : new Random(System.currentTimeMillis() + getRandomInt(10000)));
//                char c;
//                int randomInt = getRandomInt(rand, 35);
//                if(randomInt >= 26)
//                {
//                    randomInt = randomInt-26;
//                    c = (char) ('0' + randomInt);
//                }
//                else
//                {
//                   c = (char) ('a' + randomInt);
//                }
//                strbuf.append(c);
//            }
//            guid = strbuf.toString();
//        }
//        else
//        {
//            guid = UUID.randomUUID().toString();
//        }
//        return guid;
//    }

    public static int getRandomInt(int... bound)
    {
        return random.nextInt((bound != null && bound.length > 0 ? bound[0] : DEFAULT_BOUND));
    }

    private static int getRandomInt(Random random, int... bound)
    {
        return random.nextInt((bound != null && bound.length > 0 ? bound[0] : DEFAULT_BOUND));
    }

    /*public static String getString(String str, String defaultStr) {
        return ((str == null || str.trim().length() == 0) ? defaultStr : str);
    }*/
//
//    public static long getGuidlong() throws DetailedServerException
//    {
//        long longguid = 0L;
//        try
//        {
//            int maxGuidLen = ConfigManagerInstance.getInteger("maxGuidLength", 0);        
//            
//            String str = String.valueOf(System.currentTimeMillis());
//            int len = str.length();
//            if (maxGuidLen - len < 5) throw new DetailedServerException(ErrorCode.INVALID_GUID_LEN);
//            StringBuilder strbuf = new StringBuilder(maxGuidLen);
//            strbuf.append(str);
//            for (int i = len; i < maxGuidLen; i++)
//            {
//                Random rand = ((i % 2 == 0) ? random : new Random(System.currentTimeMillis() + getRandomInt(10000)));
//                char c = (char) ('0' + getRandomInt(rand, 10));
//                strbuf.append(c);
//            }
//            str = strbuf.toString();
//            if(str != null)
//            {
//                longguid = Long.parseLong(str);
//            }
//        }
//        catch (Exception e) 
//        {
//        }
//        return longguid;
//    }

    public static String getNewVersion(long... curTime)
    {
        return (curTime != null && curTime.length > 0 ? curTime[0] : System.currentTimeMillis()) + "-" + ReadUtil.getRandomInt();
    }

    // Reference: http://stackoverflow.com/questions/2106615/round-bigdecimal-to-nearest-5-cents
    public static BigDecimal round(BigDecimal value,
                                   BigDecimal increment,
                                   int roundingMode)
    {
        if (increment.signum() == 0)
        {
            // 0 increment does not make much sense, but prevent division by 0
            return value;
        }
        else
        {
            BigDecimal divided = value.divide(increment, 0, roundingMode);
            BigDecimal result = divided.multiply(increment);
            return result;
        }
    }

    public static int[] toIntArray(ArrayList<Integer> list)
    {
        int[] iRet = null;
        if (list != null)
        {
            iRet = new int[list.size()];
            int pos = 0;
            for (Integer i : list)
            {
                iRet[pos++] = i.intValue();
            }
        }
        return iRet;
    }

    public static BigDecimal toBigDecimal(String val)
    {
        BigDecimal toRet = null;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        String pattern = "#,##0.0#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);

        // parse the string
        try
        {
            toRet = (BigDecimal) decimalFormat.parse(val);
        }
        catch (Exception e)
        {}

        return toRet;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T safeGetValueFromHashmap(final HashMap<String, Object> map,
                                                final String key,
                                                final Class<T> type,
                                                final T defaultValue)
    {
        if (map != null && key != null && type != null)
        {
            if (map.containsKey(key)) { return (T) map.get(key); }
        }
        return defaultValue;
    }

    public static boolean getBooleanFromInteger(int value)
    {
        return (value > 0 ? true : false);
    }

    public static boolean getBooleanFromString(String value)
    {
        boolean toRet = false;
        try
        {
            if(value != null)
            {
                if(value.equalsIgnoreCase("true"))
                {
                    toRet = true;
                }
                else if(value.equalsIgnoreCase("false"))
                {
                    toRet = false;
                }
                else
                {
                    toRet = getBooleanFromInteger(getInt(value, 0));
                }
            }
        }
        catch (Exception e)
        {
        }
        return toRet;
    }
//    
//    public static boolean getBooleanFromObject(Object obj)
//    {
//        boolean toRet = false;
//        try
//        {
//            if(obj != null && (obj instanceof String))
//            {
//                toRet = getBooleanFromString((String) obj);
//            }
//            else if(obj != null && (obj instanceof Integer))
//            {
//                toRet = getBooleanFromInteger((int) obj);
//            }
//            else if(obj != null && (obj instanceof Boolean))
//            {
//                toRet = (boolean) obj;
//            }
//        }
//        catch (Exception e)
//        {
//        }
//        return toRet;
//    }
    public static String getSingleValue(Set<Object> set)
    {
         String toRet = null;

         if(set != null && !set.isEmpty() && set.size() == 1)
         {
             for(Object obj : set)
             {
                 toRet = obj != null ? String.valueOf(obj) : null;
                 if(toRet != null)
                 {
                     break;
                 }
             }
         }
         return toRet;
     }
    
    public static Set<Object> getObjectAsSet(Object value)
    {
        Set<Object> toRet = null;

        if(value != null)
        {
            toRet = new HashSet<Object>();
            toRet.add(value);
        }

        return toRet;
    }


    public static String[] getArrayFromCollection(Collection<String> collection)
    {
        String[] toRet = null;
        if (collection != null && collection.size() > 0)
        {
            toRet =  collection.toArray(new String[collection.size()]);
        }
        return toRet;
    }

    public static Set<String> getStringSetFromDelimitedString(Object str,
                                                              String delim,
                                                              boolean trim)
    {
        String[] strArr = StringUtil.getStrings(getString(str, null), delim, trim);
        return (strArr != null && strArr.length > 0) ? new HashSet<String>(Arrays.asList(strArr)) : new HashSet<String>();
    }

    public static Set<Object> getObjectSetFromStringOrJsonArray(Object obj,
                                                                Set<Object> defaultValue)
    {
        Set<Object> toRet = defaultValue;
        try
        {
            String data = getString(obj, null);
            if (data != null)
            {
                Object json = new JSONTokener(data).nextValue();
                if (!(json instanceof JSONObject || json instanceof JSONArray))
                {
                    JSONArray jsonArr = JSONUtils.getArrayFromStringArray(StringUtil.getStrings(getString(obj, null), ",", true));
                    if (jsonArr != null && jsonArr.length() > 0)
                    {
                        toRet = new HashSet<Object>(JSONUtils.toList(jsonArr));
                    }
                }
                else if (obj != null)
                {
                    toRet = new HashSet<Object>(Arrays.asList(obj));
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error@ReadUtil.getObjectSetFromStringOrJsonArray", e);
        }
        return toRet;
    }
    
    public static long getExpAsLongValue(String strVal, long defaultValue)
    {
        long value = defaultValue;
        if (strVal != null && ((strVal = strVal.trim()).length() > 0))
        {
            value = Double.valueOf(strVal).longValue();
        }
        return value;
    }
    public static boolean isIdenticalHashSet(Set<Object> source, Set<Object> destination) 
    {
        if ( source.size() != destination.size() ) 
        {
            return false;
        }
        HashSet<Object> clone = new HashSet<Object>(destination); 
        Iterator it = source.iterator();
        while (it.hasNext() )
        {
            Object obj = it.next();
            if (clone.contains(obj))
            { 
                clone.remove(obj);
            } 
            else 
            {
                return false;
            }
        }
        return true; 
    }

}
