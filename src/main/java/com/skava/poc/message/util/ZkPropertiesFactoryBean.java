package com.skava.poc.message.util;

import java.util.Properties;

import org.springframework.beans.factory.config.AbstractFactoryBean;

import com.skava.poc.message.util.ZookeeperHelper;

public class ZkPropertiesFactoryBean extends AbstractFactoryBean<Properties>
{
    private ZookeeperHelper zookeeperHelper;

    public ZkPropertiesFactoryBean(ZookeeperHelper zookeeperHelper)
    {
        this.zookeeperHelper = zookeeperHelper;
    }

    @Override
    protected Properties createInstance() throws Exception
    {
        Properties p = new Properties();
        p.putAll(getZookeeperHelper().loadFromZk());
        p.putAll(getZookeeperHelper().getGlobalProperties());
        return p;
    }

    @Override
    public Class<Properties> getObjectType()
    {
        return Properties.class;
    }

    /*private void loadFromLocalProps() throws IOException, InterruptedException
    {
    	FileInputStream fis = new FileInputStream(new File(localPropPath));
    	Properties p = new Properties();
    	p.load(fis);
    	Enumeration<?> propertyNames = p.propertyNames();
    	while(propertyNames.hasMoreElements())
    	{
    		String key = (String) propertyNames.nextElement();
    		String value = p.getProperty(key);
    		zk.create(path + "/" + key, value.getBytes(), Ids.OPEN_ACL_UNSAFE,
                    CreateMode.PERSISTENT);
    	}
    }*/

    public ZookeeperHelper getZookeeperHelper()
    {
        return zookeeperHelper;
    }

    public void setZookeeperHelper(ZookeeperHelper zookeeperHelper)
    {
        this.zookeeperHelper = zookeeperHelper;
    }

}
