/**
 * 
 */
package com.skava.poc.message.util;

/**
 * @author barath
 *
 */
public interface ConfigManager
{
    String get(String key);
    String get(String key, boolean skipCache);
}
