package com.skava.poc.message.util;

import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigManagerInstance
{
    private static Logger logger = LoggerFactory.getLogger(ConfigManagerInstance.class);

    private static ConfigManager configManager;

    public ConfigManagerInstance(ConfigManager cfgMgr)
    {
        configManager = cfgMgr;
    }

    /* (non-Javadoc)
     * @see com.skava.util.ConfigManager#get(java.lang.String)
     */
    public static String get(String key)
    {
        return get(key, false);
    }

    public static String get(String key, boolean skipCache)
    {
        String prop = null;
        try
        {
            prop = configManager.get(key, skipCache);
        }
        catch (Exception e)
        {
            logger.error("Unable to read property " + key);
        }
        return prop;
    }

    public static boolean getBoolean(String key, boolean defaultVal)
    {
        String propVal = get(key);
        if (propVal != null && propVal.length() > 0) { return (propVal.equals("true") || false); }
        return defaultVal;
    }
    
    public static int getInteger(String key, int defaultVal)
    {
        String propVal = get(key);
        if (propVal != null && propVal.length() > 0) { return (Integer.valueOf(propVal)); }
        return defaultVal;
    }
    
    public static long getLong(String key, long defaultVal)
    {
        String propVal = get(key);
        if (propVal != null && propVal.length() > 0) { return (Long.valueOf(propVal)); }
        return defaultVal;
    }

    public static JSONObject getJSON(String key, JSONObject defaultVal) throws JSONException
    {
        String propVal = get(key);
        if (propVal != null && propVal.trim().length() > 0) { return new JSONObject(propVal); }
        return defaultVal;
    }

    public static BigDecimal getBigDecimal(String key, BigDecimal defaultVal)
    {
        String propVal = get(key);
        if(propVal != null && propVal.length() > 0)
        {
            return ReadUtil.toBigDecimal(propVal);
        }
        return defaultVal;
    }

}
