/*******************************************************************************
 * Copyright ©2002-2014 Skava. 
 * All rights reserved.The Skava system, including 
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and 
 * other laws. Use without permission is prohibited.
 * 
 *  For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.poc.message.util;

public class Constants
{
    public static final String CTYPE_JAVASCRIPT = "text/javascript";
    public static final String CTYPE_X_JSON = "application/x-json";
    public static final String CTYPE_JSON = "application/json";
    public static final String CTYPE_OCTETS = "application/octet-stream";
    public static final String CTYPE_XML = "text/xml";
    public static final String CTYPE_HTML = "text/html";
    public static final String CTYPE_WML = "text/wml";
    public static final String CTYPE_FORM = "application/x-www-form-urlencoded";
    public static final String CTYPE_MULTIPARTFORM = "multipart/form-data";

    public static final String HTTP_HEADER_ERROR_CODE = "X-Ftips-Statuscode";
    public static final String HTTP_HEADER_ERROR_MSG = "X-Ftips-Statusmsg";
    public static final String HTTP_HEADER_CONTENT_LENGTH = "Content-Length";
    public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HTTP_HEADER_CACHE_CTRL = "Cache-Control";
    public static final String HTTP_HEADER_NO_CACHE = "no-cache";
    public static final String HTTP_HEADER_NO_CACHETRANSFORM = "no-cache, no-transform";
    public static final String HTTP_HEADER_REFERER = "referer";
    public static final String REQUEST_PATH_VARIABLE_SEPARATOR = "/";
    public static final String HTTP_HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String HTTP_COOKIE_SESSION_HTTP_ONLY = ";HttpOnly;";
    public static final String HTTP_COOKIE_TRUE_CLIENT_IP = "True-Client-IP";
    public static final String HTTP_HEADER_CORRELATION_ID = "skcid";
    
    public static final String HTTP_HEADER_COOKIE = "cookie";
    public static final String REQUEST_COOKIE_PREFIX = Constants.HTTP_HEADER_COOKIE + "_";

    public static String STREAM_SESSION_CACHE = "sessionDataCache";
    public static final String STREAMSESSIONID = "STREAMSESSIONID";
    
    public static final String CONTENT_ENCODING_GZIP = "gzip";

    public static final String METHOD_GET = "get";
    public static final String METHOD_POST = "post";
    public static final String METHOD_DELETE = "delete";
    public static final String METHOD_PUT = "put";

    public static final long ONE_DAY = 86400000L;
    public static final int WEEK_IN_SECONDS = 604800;

    public static final int MEDIATYPE_IMAGE = 1;
    public static final int MEDIATYPE_VIDEO = 2;
    public static final int MEDIATYPE_APP = 4;
    public static final int MEDIATYPE_NOTE = 5;
    public static final int MEDIATYPE_TASK = 6;
    public static final int MEDIATYPE_RSSFEED = 7;

    public static final int TYPE_CHAR = 1;
    public static final int TYPE_INT = 2;
    public static final int TYPE_STRING = 3;
    
    public static final String CURRENT_STATUS = "Current Status";
    public static final String ORDER_VERSION_V1 = "1.0";
    public static final String ORDER_VERSION_V2 = "2.0";
}
