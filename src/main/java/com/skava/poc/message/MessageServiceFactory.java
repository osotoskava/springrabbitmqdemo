package com.skava.poc.message;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageServiceFactory {
	public final static Logger log = 
			 LoggerFactory.getLogger(MessageServiceFactory.class);
	
	private MessageService messageService;
	
	private MessageServiceFactory (
			String messageBusType, 
			String url, 
			String username, 
			String password) 
			throws IOException, TimeoutException {
		
		
		if("rabbitmq".equalsIgnoreCase(messageBusType)) {
			messageService = new MessageServiceRabbitMQImpl();
			messageService.createConnection(url, username, password);
		} //TODO add other message bus types
		
	}
	
	/**
	 * Get instance of messageService class.
	 * @return
	 */
	/* Spring has initialized this
	 * class as a singleton, so it's safe to bypass any double lock condition
	 * for multithreading.
	 */
	public MessageService getInstance() {
		return messageService;
	}
	
	@PreDestroy
	public void close() throws IOException {
		log.info("CLOSING MessageService connection");
		messageService.close();
	}
}
