package com.skava.poc.message;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rabbitmq.client.Channel;


public class App {
	 public final static Logger logger = 
			 LoggerFactory.getLogger(App.class);
	
    public static void main( String[] args ) {
    	ApplicationContext context = 
                new ClassPathXmlApplicationContext("amqp-context.xml");
    	
    	MessageServiceFactory mServiceFactory = 
    			(MessageServiceFactory) context.getBean("messageService");
    	
    	MessageServiceRabbitMQ mService = 
    			(MessageServiceRabbitMQ) mServiceFactory.getInstance();
    	
    	Channel channel = mService.getChannel();
    	// FIXME list is not yet implemented, so only one exchange name exists
    	String exchange = mService.getExchange(); 
    	
    	try {
			mService.declareDirectExchangeAndBind(exchange, channel, "");
		} catch (IOException e) {
			logger.error("Unable to declare direct exchange with binding",e);
		}
    	
    	String queueName = mService.getQueueName();
    	
    	try {
			mService.putMessage(exchange, "THIS IS A TEST");
		} catch (IOException e) {
			logger.error("Unable to put message into the queue",e);
		}
    	
    	try {
			String messageFromQueue = mService.getMessage(queueName);
			logger.info("MESSAGE FROM " + queueName + " = " +messageFromQueue);
		} catch (IOException e) {
			logger.error("Unable to retrieve message from queue",e);
		}
    	
    }
}
