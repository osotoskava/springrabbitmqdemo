package com.skava.poc.message;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;
import com.rabbitmq.client.ReturnListener;

public class MessageServiceRabbitMQImpl implements MessageServiceRabbitMQ {
	public final static Logger log = 
			 LoggerFactory.getLogger(MessageServiceRabbitMQImpl.class);

	// TODO exchange names will be dynamic moving forward
	public static final String EXCHANGE = "message-service-util";
	private Connection connection;
	private Channel channel;
	private String queueName;

	public void putMessage(String exchangeName, String message) 
			throws IOException {
		log.debug("putMessage msg=("+message+"), exchange name=("+EXCHANGE+")");
		channel.basicPublish(EXCHANGE, "", null, message.getBytes());
		
	}

	public String getMessage(String queueName) throws IOException {
		log.debug("echangeName="+queueName);
		GetResponse getResponse = channel.basicGet(queueName, false);
	    return new String(getResponse.getBody());
	}
	
	public void createConnection(String url, String username, String password) 
			throws IOException {
		ConnectionFactory factory = createConnectionFactory(url, username, password);
		try {
			connection = createRabbitMQConnection(factory, url, username, password);
		} catch (TimeoutException e) {
			log.error("RabbitMQ connection retry timed out");
		}
		
		channel = createChannel(connection);
	}
	
	private ConnectionFactory createConnectionFactory(String url, String username, String password) {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(url);
        factory.setUsername(username);
        factory.setPassword(password);
        return factory;
	}

	private Connection createRabbitMQConnection(ConnectionFactory factory, 
			String url, String username, String password) 
			throws IOException, TimeoutException {
        int cores = Runtime.getRuntime().availableProcessors();
        ExecutorService es = Executors.newFixedThreadPool(cores);
        return connection = factory.newConnection(es);
	}
	
	private Channel createChannel(Connection connection) throws IOException {
		return connection.createChannel();
	}
	
	

	public void consumeMessage(Channel channel) {
		Consumer consumer = new DefaultConsumer(channel) {
	        @Override
		    public void handleDelivery(String consumerTag, Envelope envelope,
		        		AMQP.BasicProperties properties, byte[] body) throws IOException {
	       		String message = new String(body, "UTF-8");
		            log.info(" [x] Received '" + message + "'");
		    }
		 };
	}
	
	public void declareDirectExchangeAndBind(String exchange, Channel channel, String routingKey) throws IOException {
		channel.exchangeDeclare(exchange, "direct", true);
		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, EXCHANGE, routingKey);
		this.queueName = queueName;
		
//		addReturnListener(channel);
	}

	private void addReturnListener(Channel channel) {
		channel.addReturnListener(new ReturnListener() {
    	    public void handleReturn(int replyCode,
    	                                  String replyText,
    	                                  String exchange,
    	                                  String routingKey,
    	                                  AMQP.BasicProperties properties,
    	                                  byte[] body)
    	    throws IOException {
    	        log.error("Message unbounded exception");
    	    }
    	});
	}

	public Connection getConnection() {
		return connection;
	}

	public void close() throws IOException {
		try {
			channel.close();
		} catch (TimeoutException e) {
			log.error("Unable to close RabbitMQ channel #" + channel.getChannelNumber());
		}
		connection.close();
		
	}

	public Channel getChannel() {
		return channel;
	}

	public String getExchange() {
		return EXCHANGE;
	}

	public String getQueueName() {
		return queueName;
	}

}
