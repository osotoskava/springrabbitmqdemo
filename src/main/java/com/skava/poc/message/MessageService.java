package com.skava.poc.message;

import java.io.IOException;

/**
 * MessageService contains the common methods found in most AMQP, 
 * JMS and non-JMS message bus frameworks.
 *
 */
public interface MessageService {
	void createConnection(String url, String username, String password) 
			throws IOException;
	void close() throws IOException;

}
