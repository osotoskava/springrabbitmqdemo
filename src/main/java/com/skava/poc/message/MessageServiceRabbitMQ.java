package com.skava.poc.message;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * These methods are specific to RabbitMQ implementation.
 * If extends the generic MessageService interface to put and
 * consume messages.
 *
 */
public interface MessageServiceRabbitMQ extends MessageService {
	/**
	 * Create RabbitMQ connection
	 * 
	 * @param ConnectionFactory factory
	 * @param url
	 * @param username
	 * @param password
	 * @return com.rabbitmq.client.Connection
	 * @throws IOException
	 * @throws TimeoutException
	 */
//	public Connection createRabbitMQConnection(ConnectionFactory factory, 
//			String url, String username, String password)
//			throws IOException, TimeoutException;
	
	/**
	 * Returns a list of available exchanges.
	 * 
	 * @return String
	 */
	public String getExchange();
	
	/**
	 * Creates a listener for a particular channel
	 * @return
	 */
	public void consumeMessage(Channel channel);
	
	/**
	 * Returns a string representation of the body of the message
	 * @return String
	 */
	public String getMessage(String queueName) throws IOException ;
	
	/**
	 * Returns an instance of the RabbitMQ connection
	 * @return
	 */
	public Connection getConnection();
	
	/**
	 * Get channel from RabbitMQ connection
	 * @return Channel
	 */
	public Channel getChannel();
	
	/**
	 * Get queue name
	 * @return String
	 */
	public String getQueueName();
	
	/**
	 * Puts message in specified exchange
	 * @param message
	 * @throws IOException
	 */
	public void putMessage(String exchangeName, String message) throws IOException;
	
	/**
	 * Declare the exchange with type eq direct and bind the generated queue name
	 * @param channel
	 * @param routingKey
	 * @throws IOException
	 */
	public void declareDirectExchangeAndBind(String exchange, Channel channel, String routingKey) throws IOException;
	
	//TODO Create Exchange Declares for types fanout, topic and headers
	

}
